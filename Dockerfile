ARG VERSION=0.18.1

FROM bitnami/minideb:bullseye AS build

# Reuse version defined globally
ARG VERSION

ARG SOURCE_URL=https://download.litecoin.org/litecoin-${VERSION}/linux/litecoin-${VERSION}-x86_64-linux-gnu.tar.gz
ARG SIGNATURE_URL=https://download.litecoin.org/litecoin-${VERSION}/linux/litecoin-${VERSION}-linux-signatures.asc

WORKDIR /build

# Install needed packages
RUN apt-get update && \
    apt-get install -y ca-certificates=20210119 wget=1.21-1+b1 gpg=2.2.27-2 dirmngr=2.2.27-2 gpg-agent=2.2.27-2 --no-install-recommends

# Download litecoin binary and GPG signatures
RUN wget --progress=dot:giga $SOURCE_URL && \
    wget --progress=dot:giga $SIGNATURE_URL && \
    tar -xf litecoin-${VERSION}-x86_64-linux-gnu.tar.gz

# Check hash and signature
RUN gpg --keyserver keyserver.ubuntu.com --recv-keys FE3348877809386C && \
    sha256sum --check --ignore-missing litecoin-${VERSION}-linux-signatures.asc && \
    gpg --verify litecoin-${VERSION}-linux-signatures.asc

# Slimmed down image with small attack surface
FROM gcr.io/distroless/cc-debian11:3d9d96688716c922c7fe727b457d3d402eb4b1c4

# Reuse version defined globally
ARG VERSION

# Copy binary from build stage
COPY --from=build /build/litecoin-${VERSION} /app

# Run as nonroot
USER nonroot

ENTRYPOINT ["/app/bin/litecoind", "-datadir=/tmp"]   